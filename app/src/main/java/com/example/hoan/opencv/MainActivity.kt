package com.example.hoan.opencv

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.graphics.Bitmap
import android.provider.ContactsContract
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import org.opencv.android.Utils
import org.opencv.imgproc.Imgproc
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.OpenCVLoader
import org.opencv.core.*
import java.util.*
import org.opencv.calib3d.Calib3d
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc.threshold
import android.R.attr.y
import android.R.attr.x
import org.opencv.core.Scalar
import org.opencv.core.MatOfPoint
import org.opencv.core.MatOfInt
import org.opencv.imgproc.Imgproc.threshold
import android.R.attr.y
import android.R.attr.x
import java.time.Instant.MAX
import java.time.Instant.MIN


class MainActivity : AppCompatActivity() {
    var anhgoc:Mat?=null
    var anhshow:Mat?=null



    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.i("Load", "OpenCV loaded successfully");
                    anhgoc = Mat()
                    anhshow= Mat()

                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       btnChuyendoi.setOnClickListener {
           anhgoc= Utils.loadResource(baseContext,R.drawable.demo,Imgcodecs.IMREAD_COLOR) // đọc ảnh gốc
           Contours()

       }

    }

    internal fun Contours() {
        val grayMat = Mat()
        val cannyEdges = Mat()
        val threshold= Mat()
        val hierarchy = Mat()

        val contourList = ArrayList<MatOfPoint>() //A list to store all the contours


        //chuyển ảnh gốc sang ảnh xám
        Imgproc.cvtColor(anhgoc, grayMat, Imgproc.COLOR_BGR2GRAY)
        Imgproc.threshold(grayMat,threshold,80.0,255.0, Imgproc.THRESH_BINARY_INV) // phân ngưỡng hình ảnh


        // tìm kiếm đường bao
        Imgproc.findContours(threshold, contourList, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE)//CHAIN_APPROX_SIMPLE

        //vẽ đường viền trên hình ảnh mới
        val contours = Mat() // Đường bao
        contours.create(threshold.rows(), threshold.cols(), CvType.CV_8UC3)

        val newImage = Mat()
        anhgoc?.copyTo(newImage)
        for (i in contourList.indices) {
            if (Imgproc.contourArea(contourList.get(i)) > 5000)
                Imgproc.drawContours(newImage,contourList,i,Scalar(223.0,0.0,41.0),4 )
        }

        //chuyển hình ảnh từ ma trận về bitmap
        var bm :Bitmap = Bitmap.createBitmap(threshold!!.cols(),threshold!!.rows(),Bitmap.Config.ARGB_8888)

        Utils.matToBitmap(newImage, bm)
        imvConvert.setImageBitmap(bm)
    }

    fun findCard(){
        var img1= Mat()
        anhgoc?.copyTo(img1)
        val sz = Size(960.0, 720.0)
        if(img1.cols()>img1.rows()){
            val sz = Size(960.0, 720.0)
            Imgproc.resize(img1,anhgoc,sz,2.0)
        }else{
            val sz = Size(720.0, 960.0)
            Imgproc.resize(img1,anhgoc,sz,2.0)
        }

        var imgx=Mat()
        anhgoc?.copyTo(imgx)
        var img2=Mat()
        var img11=Mat()
        Imgproc.medianBlur(imgx,img11,13)
        Imgproc.cvtColor(img11,img11,Imgproc.COLOR_GRAY2RGB)
        Imgproc.adaptiveThreshold(img11,img2,255.0,Imgproc.ADAPTIVE_THRESH_MEAN_C,Imgproc.THRESH_BINARY,15,1.0)
        val sigma = 0.33
        val v =Imgproc.CV_MEDIAN
      
       // val upper = Int(MIN(255, (1.0 + sigma)*v))

    }




    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {

            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {

            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }


}
